from django.urls import path

from todos.views import (
    TodoListListView,
    TodoListUpdateView,
    TodoListCreateView,
    TodoListDetailView,
    TodoItemCreateView,
)

urlpatterns = [
    path("", TodoListListView.as_view(), name="todo_list_list"),
    path("<int:pk>/", TodoListDetailView.as_view(), name="todo_list_detail"),
    path("create/", TodoListCreateView.as_view(), name="todo_list_create"),
    path("<int:pk>", TodoListUpdateView.as_view(), name="todo_list_update",),
    path("items/create/", TodoItemCreateView.as_view(), name="todo_item_create"),
]